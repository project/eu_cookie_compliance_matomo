EU Cookie Compliance Matomo module for Drupal.

INTRODUCTION
------------
Allows you to manage Cookie compilance for Matomo.
It works with cookie categories:
* Opt-in.
Don't track visitors unless they specifically give consent. (GDPR compliant)
* Opt-in with categories.
* Let visitors choose which cookie categories they want to opt-in for
(GDPR compliant).

REQUIREMENTS
------------
This module require:
- EU Cookie Compliance module
- Matomo

INSTALLATION INSTRUCTIONS
-------------------------
* Install as you would normally install a contributed Drupal module. Visit:
  https://drupal.org/documentation/install/modules-themes/modules-8
  for further information.

CONFIGURATION
-------------

* Go to /admin/config/system/eu-cookie-compliance/matomo
* Choose categories used to track users with Matomo.
