<?php

namespace Drupal\eu_cookie_compliance_matomo\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides settings for eu_cookie_compliance_matomo module.
 */
class EuCookieComplianceMatomoForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * The Cookie Category Storage Manager.
   *
   * @var \Drupal\eu_cookie_compliance\CategoryStorageManager
   */
  protected $categoryStorageManager;

  /**
   * Constructs an EuCookieComplianceMatomoForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The filter format storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);

    $this->categoryStorageManager = $entity_type_manager->getStorage('cookie_category');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eu_cookie_compliance_matomo_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'eu_cookie_compliance_matomo.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('eu_cookie_compliance_matomo.settings');

    $form['consent'] = [
      '#type' => 'details',
      '#title' => $this->t('Consent for processing of personal information'),
      '#open' => TRUE,
    ];

    $form['consent']['info'] = [
      '#type' => 'markup',
      '#markup' => $this->t("Choose categories for opt-in with categories. Let visitors choose which cookie categories they want to opt-in for (GDPR compliant)."),
    ];

    $form['consent']['categories'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Categories'),
      '#description' => $this->t("Select categories for opt-in with categories."),
      '#options' => $this->getCategories(),
      '#default_value' => $config->get('categories') ? $config->get('categories') : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Save settings.
    $this->config('eu_cookie_compliance_matomo.settings')
      ->set('categories', $form_state->getValue('categories'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get categories.
   *
   * @return array
   *   Categories
   */
  private function getCategories() {
    $categories = [];
    $cookie_categories = $this->categoryStorageManager->getCookieCategories();
    foreach ($cookie_categories as $key => $cookie_category) {
      $categories[$key] = isset($cookie_category['label']) ? $cookie_category['label'] : NULL;
    }

    return $categories;
  }

}
